﻿namespace DavidClient
{
    partial class CreateRoomMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.createRoomButton = new System.Windows.Forms.Button();
            this.maxPlayersTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.roomNameTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.errorLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.easyDifficultyRadioButton = new System.Windows.Forms.RadioButton();
            this.hardDifficultyRadioButton = new System.Windows.Forms.RadioButton();
            this.goBackButton = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.goBackButton)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("MV Boli", 50F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Silver;
            this.label1.Location = new System.Drawing.Point(169, -8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(453, 87);
            this.label1.TabIndex = 24;
            this.label1.Text = "Create Room";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Calibri", 15F);
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(521, 74);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 24);
            this.label8.TabIndex = 45;
            // 
            // createRoomButton
            // 
            this.createRoomButton.BackColor = System.Drawing.SystemColors.Control;
            this.createRoomButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.createRoomButton.Font = new System.Drawing.Font("Britannic Bold", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createRoomButton.Location = new System.Drawing.Point(283, 328);
            this.createRoomButton.Name = "createRoomButton";
            this.createRoomButton.Size = new System.Drawing.Size(205, 113);
            this.createRoomButton.TabIndex = 44;
            this.createRoomButton.Text = "Create";
            this.createRoomButton.UseVisualStyleBackColor = false;
            this.createRoomButton.Click += new System.EventHandler(this.createRoomButton_Click);
            // 
            // maxPlayersTextBox
            // 
            this.maxPlayersTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.maxPlayersTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.maxPlayersTextBox.Location = new System.Drawing.Point(185, 175);
            this.maxPlayersTextBox.Name = "maxPlayersTextBox";
            this.maxPlayersTextBox.Size = new System.Drawing.Size(390, 23);
            this.maxPlayersTextBox.TabIndex = 43;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Britannic Bold", 25F);
            this.label4.ForeColor = System.Drawing.Color.Silver;
            this.label4.Location = new System.Drawing.Point(178, 135);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(303, 37);
            this.label4.TabIndex = 39;
            this.label4.Text = "Max Players (2 - 4)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Calibri", 15F);
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(279, 69);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 24);
            this.label6.TabIndex = 37;
            // 
            // roomNameTextBox
            // 
            this.roomNameTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.roomNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.roomNameTextBox.Location = new System.Drawing.Point(185, 109);
            this.roomNameTextBox.Name = "roomNameTextBox";
            this.roomNameTextBox.Size = new System.Drawing.Size(390, 23);
            this.roomNameTextBox.TabIndex = 36;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Britannic Bold", 25F);
            this.label2.ForeColor = System.Drawing.Color.Silver;
            this.label2.Location = new System.Drawing.Point(177, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(215, 37);
            this.label2.TabIndex = 35;
            this.label2.Text = "Room Name: ";
            // 
            // errorLabel
            // 
            this.errorLabel.AutoSize = true;
            this.errorLabel.BackColor = System.Drawing.Color.Transparent;
            this.errorLabel.Font = new System.Drawing.Font("Britannic Bold", 25F);
            this.errorLabel.ForeColor = System.Drawing.Color.Red;
            this.errorLabel.Location = new System.Drawing.Point(12, 404);
            this.errorLabel.Name = "errorLabel";
            this.errorLabel.Size = new System.Drawing.Size(0, 37);
            this.errorLabel.TabIndex = 46;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Britannic Bold", 25F);
            this.label3.ForeColor = System.Drawing.Color.Silver;
            this.label3.Location = new System.Drawing.Point(305, 201);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(162, 37);
            this.label3.TabIndex = 39;
            this.label3.Text = "Difficulty:";
            // 
            // easyDifficultyRadioButton
            // 
            this.easyDifficultyRadioButton.AutoSize = true;
            this.easyDifficultyRadioButton.BackColor = System.Drawing.Color.Transparent;
            this.easyDifficultyRadioButton.Font = new System.Drawing.Font("Britannic Bold", 18F);
            this.easyDifficultyRadioButton.ForeColor = System.Drawing.Color.Black;
            this.easyDifficultyRadioButton.Location = new System.Drawing.Point(184, 236);
            this.easyDifficultyRadioButton.Name = "easyDifficultyRadioButton";
            this.easyDifficultyRadioButton.Size = new System.Drawing.Size(78, 31);
            this.easyDifficultyRadioButton.TabIndex = 47;
            this.easyDifficultyRadioButton.TabStop = true;
            this.easyDifficultyRadioButton.Text = "Easy";
            this.easyDifficultyRadioButton.UseVisualStyleBackColor = false;
            // 
            // hardDifficultyRadioButton
            // 
            this.hardDifficultyRadioButton.AutoSize = true;
            this.hardDifficultyRadioButton.BackColor = System.Drawing.Color.Transparent;
            this.hardDifficultyRadioButton.Font = new System.Drawing.Font("Britannic Bold", 18F);
            this.hardDifficultyRadioButton.ForeColor = System.Drawing.Color.Black;
            this.hardDifficultyRadioButton.Location = new System.Drawing.Point(184, 273);
            this.hardDifficultyRadioButton.Name = "hardDifficultyRadioButton";
            this.hardDifficultyRadioButton.Size = new System.Drawing.Size(82, 31);
            this.hardDifficultyRadioButton.TabIndex = 47;
            this.hardDifficultyRadioButton.TabStop = true;
            this.hardDifficultyRadioButton.Text = "Hard";
            this.hardDifficultyRadioButton.UseVisualStyleBackColor = false;
            // 
            // goBackButton
            // 
            this.goBackButton.BackColor = System.Drawing.Color.Transparent;
            this.goBackButton.BackgroundImage = global::DavidClient.Properties.Resources.go_back;
            this.goBackButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.goBackButton.Location = new System.Drawing.Point(0, 1);
            this.goBackButton.Name = "goBackButton";
            this.goBackButton.Size = new System.Drawing.Size(69, 63);
            this.goBackButton.TabIndex = 61;
            this.goBackButton.TabStop = false;
            this.goBackButton.Click += new System.EventHandler(this.goBackButton_Click);
            // 
            // CreateRoomMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.goBackButton);
            this.Controls.Add(this.hardDifficultyRadioButton);
            this.Controls.Add(this.easyDifficultyRadioButton);
            this.Controls.Add(this.errorLabel);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.createRoomButton);
            this.Controls.Add(this.maxPlayersTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.roomNameTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "CreateRoomMenu";
            this.Text = "createRoomMenu";
            ((System.ComponentModel.ISupportInitialize)(this.goBackButton)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button createRoomButton;
        private System.Windows.Forms.TextBox maxPlayersTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox roomNameTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label errorLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton easyDifficultyRadioButton;
        private System.Windows.Forms.RadioButton hardDifficultyRadioButton;
        private System.Windows.Forms.PictureBox goBackButton;
    }
}