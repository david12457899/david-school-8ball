﻿namespace DavidClient
{
    partial class Game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SubmitBoardButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.playerPanel3 = new System.Windows.Forms.Panel();
            this.playerPanel2 = new System.Windows.Forms.Panel();
            this.playerPanel1 = new System.Windows.Forms.Panel();
            this.mainGamePanel = new System.Windows.Forms.Panel();
            this.nameLabel = new System.Windows.Forms.Label();
            this.otherPlayerLabel1 = new System.Windows.Forms.Label();
            this.otherPlayerLabel2 = new System.Windows.Forms.Label();
            this.otherPlayerLabel3 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timeLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // SubmitBoardButton
            // 
            this.SubmitBoardButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.SubmitBoardButton.Location = new System.Drawing.Point(326, 481);
            this.SubmitBoardButton.Name = "SubmitBoardButton";
            this.SubmitBoardButton.Size = new System.Drawing.Size(142, 36);
            this.SubmitBoardButton.TabIndex = 9;
            this.SubmitBoardButton.Text = "Submit Board";
            this.SubmitBoardButton.UseVisualStyleBackColor = true;
            this.SubmitBoardButton.Click += new System.EventHandler(this.SubmitBoardButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(302, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(206, 33);
            this.label2.TabIndex = 8;
            this.label2.Text = "Beat Them All!";
            // 
            // playerPanel3
            // 
            this.playerPanel3.Location = new System.Drawing.Point(891, 416);
            this.playerPanel3.Name = "playerPanel3";
            this.playerPanel3.Size = new System.Drawing.Size(198, 198);
            this.playerPanel3.TabIndex = 4;
            // 
            // playerPanel2
            // 
            this.playerPanel2.Location = new System.Drawing.Point(891, 212);
            this.playerPanel2.Name = "playerPanel2";
            this.playerPanel2.Size = new System.Drawing.Size(198, 198);
            this.playerPanel2.TabIndex = 5;
            // 
            // playerPanel1
            // 
            this.playerPanel1.Location = new System.Drawing.Point(891, 8);
            this.playerPanel1.Name = "playerPanel1";
            this.playerPanel1.Size = new System.Drawing.Size(198, 198);
            this.playerPanel1.TabIndex = 6;
            // 
            // mainGamePanel
            // 
            this.mainGamePanel.Location = new System.Drawing.Point(227, 115);
            this.mainGamePanel.Name = "mainGamePanel";
            this.mainGamePanel.Size = new System.Drawing.Size(360, 360);
            this.mainGamePanel.TabIndex = 7;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.BackColor = System.Drawing.Color.Transparent;
            this.nameLabel.Font = new System.Drawing.Font("Britannic Bold", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.nameLabel.Location = new System.Drawing.Point(12, 9);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(0, 37);
            this.nameLabel.TabIndex = 21;
            // 
            // otherPlayerLabel1
            // 
            this.otherPlayerLabel1.AutoSize = true;
            this.otherPlayerLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.otherPlayerLabel1.Location = new System.Drawing.Point(635, 8);
            this.otherPlayerLabel1.Name = "otherPlayerLabel1";
            this.otherPlayerLabel1.Size = new System.Drawing.Size(0, 22);
            this.otherPlayerLabel1.TabIndex = 22;
            // 
            // otherPlayerLabel2
            // 
            this.otherPlayerLabel2.AutoSize = true;
            this.otherPlayerLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.otherPlayerLabel2.Location = new System.Drawing.Point(635, 212);
            this.otherPlayerLabel2.Name = "otherPlayerLabel2";
            this.otherPlayerLabel2.Size = new System.Drawing.Size(0, 22);
            this.otherPlayerLabel2.TabIndex = 22;
            // 
            // otherPlayerLabel3
            // 
            this.otherPlayerLabel3.AutoSize = true;
            this.otherPlayerLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.otherPlayerLabel3.Location = new System.Drawing.Point(635, 416);
            this.otherPlayerLabel3.Name = "otherPlayerLabel3";
            this.otherPlayerLabel3.Size = new System.Drawing.Size(0, 22);
            this.otherPlayerLabel3.TabIndex = 22;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timeLabel
            // 
            this.timeLabel.AutoSize = true;
            this.timeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F);
            this.timeLabel.Location = new System.Drawing.Point(359, 546);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(71, 29);
            this.timeLabel.TabIndex = 23;
            this.timeLabel.Text = "00:00";
            // 
            // Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1101, 631);
            this.Controls.Add(this.timeLabel);
            this.Controls.Add(this.otherPlayerLabel3);
            this.Controls.Add(this.otherPlayerLabel2);
            this.Controls.Add(this.otherPlayerLabel1);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.SubmitBoardButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.playerPanel3);
            this.Controls.Add(this.playerPanel2);
            this.Controls.Add(this.playerPanel1);
            this.Controls.Add(this.mainGamePanel);
            this.Name = "Game";
            this.Text = "Game";
            this.Load += new System.EventHandler(this.Game_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SubmitBoardButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel playerPanel3;
        private System.Windows.Forms.Panel playerPanel2;
        private System.Windows.Forms.Panel playerPanel1;
        private System.Windows.Forms.Panel mainGamePanel;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label otherPlayerLabel1;
        private System.Windows.Forms.Label otherPlayerLabel2;
        private System.Windows.Forms.Label otherPlayerLabel3;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label timeLabel;
    }
}