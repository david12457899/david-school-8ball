﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DavidClient
{
    public partial class CreateRoomMenu : Form
    {
        private string _name;
        private Socket _clientSocket;
        public CreateRoomMenu(string name, Socket clientSocket)
        {
            InitializeComponent();
            _clientSocket = clientSocket;
            _name = name;
        }
        
        private string createCreateRoomMessage(string roomName, string playerName, int maxPlayers, string difficulty)
        {
            return "CREATE_ROOM_REQUEST" + ";" + roomName + ";" + playerName + ";" + maxPlayers.ToString() + ";" + difficulty;
        }
        private void sendCreateRoomMessage(string roomName, string playerName, int maxPlayers, string difficulty)
        {
            // Send room details to the server
            string newRoomDetails = createCreateRoomMessage(roomName, playerName, maxPlayers, difficulty);
            byte[] userInfo = Encoding.ASCII.GetBytes(newRoomDetails);
            _clientSocket.Send(userInfo);

            // Receive message from the server
            byte[] buffer = new byte[1024];
            int rec = _clientSocket.Receive(buffer);
            byte[] temp = new byte[rec];
            Array.Copy(buffer, temp, rec);
            string serverResult = Encoding.ASCII.GetString(temp);

            if (serverResult == "CREATE_ROOM_SUCCESS")
            {
                this.Hide();
                waitingRoomAdmin waitingRoomAdminForm = new waitingRoomAdmin(playerName, roomName, maxPlayers, _clientSocket);
                waitingRoomAdminForm.Closed += (s, args) => this.Close();
                waitingRoomAdminForm.Show();
            }
            else
            {
                // TODO: This is where you know that a room with that name already exists
            }

        }

        private bool checkValidBoxes()
        {
            if (this.roomNameTextBox.Text == "" || this.maxPlayersTextBox.Text == "")
            {
                this.errorLabel.Text = "Both text boxes need to have a value";
                return false;
            }
            
            if(this.roomNameTextBox.Text.Length == 0 || this.roomNameTextBox.Text.Length > 30)
            {
                this.errorLabel.Text = "A room name has to be 0-30 characters long";
                return false;
            }

            int playerInRoom;
            
            if(int.TryParse(maxPlayersTextBox.Text, out playerInRoom) &&
                playerInRoom > 4 || playerInRoom < 2)
            {
                this.errorLabel.Text = "2-4 Players in a room.";
                return false;
            }
            else if(!int.TryParse(maxPlayersTextBox.Text, out playerInRoom))
            {
                this.errorLabel.Text = "Players in a room must be a number";
                return false;
            }
            
            if(!easyDifficultyRadioButton.Checked && !hardDifficultyRadioButton.Checked)
            {
                this.errorLabel.Text = "Players in a room must be a number";
                return false;
            }

            return true;
        }


        private void createRoomButton_Click(object sender, EventArgs e)
        {
            if(checkValidBoxes())
            {
                sendServerCreateRoom();
            }
        }

        private void sendServerCreateRoom()
        {
            string roomName = this.roomNameTextBox.Text;
            string playerName = this._name;
            int maxPlayers = int.Parse(this.maxPlayersTextBox.Text);
            string difficulty = easyDifficultyRadioButton.Checked ? "EASY" : "HARD";

            sendCreateRoomMessage(roomName, playerName, maxPlayers, difficulty);
        }

        private void goBackButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainMenu mainMenuForm = new MainMenu(_name, _clientSocket);
            mainMenuForm.Closed += (s, args) => this.Close();
            mainMenuForm.Show();
        }
    }
}
