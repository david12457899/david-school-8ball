﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DavidClient
{
    class SudokuCell : Button
    {
        public int _Value { get; set; }
        public bool _IsLocked { get; set; }
        public int _X { get; set; }
        public int _Y { get; set; }

        public void Clear()
        {
            this.Text = string.Empty;
            this._IsLocked = false;
        }
    }
}
