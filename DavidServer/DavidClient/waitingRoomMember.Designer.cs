﻿namespace DavidClient
{
    partial class waitingRoomMember
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameLabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.maxPlayersLabel = new System.Windows.Forms.Label();
            this.roomNameLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.goBackButton = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.goBackButton)).BeginInit();
            this.SuspendLayout();
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.BackColor = System.Drawing.Color.Transparent;
            this.nameLabel.Font = new System.Drawing.Font("Britannic Bold", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.nameLabel.Location = new System.Drawing.Point(30, 180);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(120, 37);
            this.nameLabel.TabIndex = 75;
            this.nameLabel.Text = "Name: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.label6.Font = new System.Drawing.Font("Britannic Bold", 20F);
            this.label6.ForeColor = System.Drawing.Color.Silver;
            this.label6.Location = new System.Drawing.Point(35, 226);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(267, 30);
            this.label6.TabIndex = 74;
            this.label6.Text = "Current Participants:";
            // 
            // maxPlayersLabel
            // 
            this.maxPlayersLabel.AutoSize = true;
            this.maxPlayersLabel.BackColor = System.Drawing.Color.Transparent;
            this.maxPlayersLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this.maxPlayersLabel.Font = new System.Drawing.Font("Britannic Bold", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxPlayersLabel.ForeColor = System.Drawing.Color.Silver;
            this.maxPlayersLabel.Location = new System.Drawing.Point(30, 143);
            this.maxPlayersLabel.Name = "maxPlayersLabel";
            this.maxPlayersLabel.Size = new System.Drawing.Size(216, 37);
            this.maxPlayersLabel.TabIndex = 72;
            this.maxPlayersLabel.Text = "Max Players: ";
            // 
            // roomNameLabel
            // 
            this.roomNameLabel.AutoSize = true;
            this.roomNameLabel.BackColor = System.Drawing.Color.Transparent;
            this.roomNameLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this.roomNameLabel.Font = new System.Drawing.Font("Britannic Bold", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.roomNameLabel.ForeColor = System.Drawing.Color.Silver;
            this.roomNameLabel.Location = new System.Drawing.Point(33, 107);
            this.roomNameLabel.Name = "roomNameLabel";
            this.roomNameLabel.Size = new System.Drawing.Size(102, 37);
            this.roomNameLabel.TabIndex = 71;
            this.roomNameLabel.Text = "Room";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("MV Boli", 55F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Silver;
            this.label1.Location = new System.Drawing.Point(215, -2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(527, 97);
            this.label1.TabIndex = 68;
            this.label1.Text = "Hello Member";
            // 
            // goBackButton
            // 
            this.goBackButton.BackColor = System.Drawing.Color.Transparent;
            this.goBackButton.BackgroundImage = global::DavidClient.Properties.Resources.go_back;
            this.goBackButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.goBackButton.Location = new System.Drawing.Point(-1, -2);
            this.goBackButton.Name = "goBackButton";
            this.goBackButton.Size = new System.Drawing.Size(69, 63);
            this.goBackButton.TabIndex = 76;
            this.goBackButton.TabStop = false;
            this.goBackButton.Click += new System.EventHandler(this.goBackButton_Click);
            // 
            // waitingRoomMember
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(862, 546);
            this.Controls.Add(this.goBackButton);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.maxPlayersLabel);
            this.Controls.Add(this.roomNameLabel);
            this.Controls.Add(this.label1);
            this.Name = "waitingRoomMember";
            this.Text = "waitingRoomMember";
            this.Load += new System.EventHandler(this.waitingRoomMember_Load);
            ((System.ComponentModel.ISupportInitialize)(this.goBackButton)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label maxPlayersLabel;
        private System.Windows.Forms.Label roomNameLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox goBackButton;
    }
}