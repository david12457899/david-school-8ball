﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;

// TODO: Fix the green not disappearing when zero key is pressed problem
namespace DavidClient
{
    class Board
    {
        private Panel _sudokuPanel;
        private SudokuCell[,] _cells = new SudokuCell[9, 9];
        private int _buttonSize;
        private bool _isMainBoard;
        private string _puzzle;
        Socket _clientSocket;

        // Variables for talking to the server
        string _roomName;
        string _playerName;

        public Board(string roomName, string playerName, Panel sudokuPanel, bool _isMainBoard, string puzzle, Socket clientSocket)
        {
            _roomName = roomName;
            _playerName = playerName;
            this._sudokuPanel = sudokuPanel;
            this._buttonSize = sudokuPanel.Size.Width / 9;
            this._isMainBoard = _isMainBoard;
            _puzzle = puzzle;
            _clientSocket = clientSocket;

            createCells();
            fillPuzzleStart(_puzzle);

        }
        private void createCells()
        {
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    // Create 81 _cells for with styles and locations based on the index
                    _cells[i, j] = new SudokuCell();
                    _cells[i, j].Font = new Font(SystemFonts.DefaultFont.FontFamily, _isMainBoard ? 20 : 8);
                    _cells[i, j].Size = new Size(this._buttonSize, this._buttonSize);
                    _cells[i, j].ForeColor = SystemColors.ControlDarkDark;
                    _cells[i, j].Location = new Point(j * this._buttonSize, i * this._buttonSize);
                    _cells[i, j].BackColor = ((i / 3) + (j / 3)) % 2 == 0 ? SystemColors.Control : Color.LightGray;
                    _cells[i, j].FlatStyle = FlatStyle.Flat;
                    _cells[i, j].FlatAppearance.BorderColor = Color.Black;
                    _cells[i, j]._X = i;
                    _cells[i, j]._Y = j;

                    // Assign key press event for each _cells
                    _cells[i, j].KeyPress += cell_keyPressed;

                    this._sudokuPanel.Controls.Add(_cells[i, j]);
                }
            }
        }
        private void fillPuzzleStart(string puzzle)
        {
            int i, j;

            for (int puzzleIndex = 0; puzzleIndex < 81; puzzleIndex++)
            {
                i = puzzleIndex / 9;
                j = puzzleIndex % 9;

                if (puzzle[puzzleIndex] != '.')
                {
                    if (_isMainBoard)
                    {
                        _cells[i, j].Text = puzzle[puzzleIndex].ToString();
                        _cells[i, j]._IsLocked = true;
                    }
                    else if (puzzle[puzzleIndex] != '.')
                    {
                        _cells[i, j].BackColor = Color.Green;
                    }
                    
                }
                else
                {
                    _cells[i, j].BackColor = ((i / 3) + (j / 3)) % 2 == 0 ? SystemColors.Control : Color.LightGray;
                }
            }
        }
        private void cell_keyPressed(object sender, KeyPressEventArgs e)
        {
            var cell = sender as SudokuCell;

            // Do nothing if the cell is locked
            if (cell._IsLocked || !_isMainBoard)
                return;

            int value;

            // Add the pressed key value in the cell only if it is a number
            if (int.TryParse(e.KeyChar.ToString(), out value))
            {
                // Clear the cell value if pressed key is zero
                if (value == 0)
                {
                    cell.Clear();
                }
                else
                {
                    cell.Text = value.ToString();
                }

                updateStringPuzzle(cell);
                sendKeyPress();

                cell.ForeColor = SystemColors.ControlDarkDark;
            }
        }
        
        private void updateStringPuzzle(SudokuCell cell)
        {
            int counter = 0;
            bool keepRunning = true;

            for(int i =0; i < 9 && keepRunning; i++)
            {
                for(int j = 0; j < 9 && keepRunning; j++)
                {
                    if(_cells[i, j] == cell)
                    {
                        keepRunning = false;
                    }
                    else
                    {
                        counter++;
                    }
                }
            }

            // Change the number in counter position (using string builder because strings are immutable)
            StringBuilder sb = new StringBuilder(_puzzle);
            sb[counter] = cell.Text.Length != 0 ? char.Parse(cell.Text) : '.';
            _puzzle = sb.ToString();
        }

        private string createSendKeyPressedMessage()
        {
            return "KEY_PRESSED_NOTIFY" + ";" + _roomName + ";" + _playerName + ";" + _puzzle;
        }
        private void sendKeyPress()
        {
            string pressedKeyMessage = createSendKeyPressedMessage();
            byte[] userInfo = Encoding.ASCII.GetBytes(pressedKeyMessage);
            _clientSocket.Send(userInfo);
        }
        
        public void updatePuzzle(string puzzle)
        {
            // TODO: Check if there's a need for line 157 or dealing with _puzzle in boards that are not the main board in general
            _puzzle = puzzle;
            fillPuzzleStart(puzzle);
        }

        public string getPuzzle()
        {
            return _puzzle;
        }

        public SudokuCell[,] getCells()
        {
            return this._cells;
        }
    }
}
