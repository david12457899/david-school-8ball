﻿namespace DavidClient
{
    partial class RoomListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.roomsPanel = new System.Windows.Forms.Panel();
            this.refreshButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.joinButton = new System.Windows.Forms.Button();
            this.playersInRoomHeadlineLabel = new System.Windows.Forms.Label();
            this.maxPlayersLabel = new System.Windows.Forms.Label();
            this.playersInRoomLabel = new System.Windows.Forms.Label();
            this.roundTimeSecondsLabel = new System.Windows.Forms.Label();
            this.updateRoomsTimer = new System.Windows.Forms.Timer(this.components);
            this.goBackButton = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.goBackButton)).BeginInit();
            this.SuspendLayout();
            // 
            // roomsPanel
            // 
            this.roomsPanel.AutoScroll = true;
            this.roomsPanel.Location = new System.Drawing.Point(218, 88);
            this.roomsPanel.Name = "roomsPanel";
            this.roomsPanel.Size = new System.Drawing.Size(373, 266);
            this.roomsPanel.TabIndex = 38;
            // 
            // refreshButton
            // 
            this.refreshButton.BackColor = System.Drawing.SystemColors.Control;
            this.refreshButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.refreshButton.Font = new System.Drawing.Font("Britannic Bold", 25F);
            this.refreshButton.Location = new System.Drawing.Point(218, 360);
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Size = new System.Drawing.Size(176, 67);
            this.refreshButton.TabIndex = 37;
            this.refreshButton.Text = "Refresh";
            this.refreshButton.UseVisualStyleBackColor = false;
            this.refreshButton.Click += new System.EventHandler(this.refreshButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("MV Boli", 50F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Silver;
            this.label1.Location = new System.Drawing.Point(194, -2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(423, 87);
            this.label1.TabIndex = 36;
            this.label1.Text = "Rooms List:";
            // 
            // joinButton
            // 
            this.joinButton.BackColor = System.Drawing.SystemColors.Control;
            this.joinButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.joinButton.Font = new System.Drawing.Font("Britannic Bold", 25F);
            this.joinButton.Location = new System.Drawing.Point(415, 360);
            this.joinButton.Name = "joinButton";
            this.joinButton.Size = new System.Drawing.Size(176, 67);
            this.joinButton.TabIndex = 39;
            this.joinButton.Text = "Join";
            this.joinButton.UseVisualStyleBackColor = false;
            this.joinButton.Click += new System.EventHandler(this.joinButton_Click);
            // 
            // playersInRoomHeadlineLabel
            // 
            this.playersInRoomHeadlineLabel.AutoSize = true;
            this.playersInRoomHeadlineLabel.BackColor = System.Drawing.Color.Transparent;
            this.playersInRoomHeadlineLabel.Font = new System.Drawing.Font("MV Boli", 15F, System.Drawing.FontStyle.Bold);
            this.playersInRoomHeadlineLabel.ForeColor = System.Drawing.Color.Silver;
            this.playersInRoomHeadlineLabel.Location = new System.Drawing.Point(597, 85);
            this.playersInRoomHeadlineLabel.Name = "playersInRoomHeadlineLabel";
            this.playersInRoomHeadlineLabel.Size = new System.Drawing.Size(198, 26);
            this.playersInRoomHeadlineLabel.TabIndex = 40;
            this.playersInRoomHeadlineLabel.Text = "PLayers In Room:";
            // 
            // maxPlayersLabel
            // 
            this.maxPlayersLabel.AutoSize = true;
            this.maxPlayersLabel.BackColor = System.Drawing.Color.Transparent;
            this.maxPlayersLabel.Font = new System.Drawing.Font("MV Boli", 15F, System.Drawing.FontStyle.Bold);
            this.maxPlayersLabel.ForeColor = System.Drawing.Color.Silver;
            this.maxPlayersLabel.Location = new System.Drawing.Point(597, 249);
            this.maxPlayersLabel.Name = "maxPlayersLabel";
            this.maxPlayersLabel.Size = new System.Drawing.Size(164, 26);
            this.maxPlayersLabel.TabIndex = 41;
            this.maxPlayersLabel.Text = "Max  PLayers:";
            // 
            // playersInRoomLabel
            // 
            this.playersInRoomLabel.AutoSize = true;
            this.playersInRoomLabel.BackColor = System.Drawing.Color.Transparent;
            this.playersInRoomLabel.Font = new System.Drawing.Font("MV Boli", 15F, System.Drawing.FontStyle.Bold);
            this.playersInRoomLabel.ForeColor = System.Drawing.Color.Transparent;
            this.playersInRoomLabel.Location = new System.Drawing.Point(597, 111);
            this.playersInRoomLabel.Name = "playersInRoomLabel";
            this.playersInRoomLabel.Size = new System.Drawing.Size(0, 26);
            this.playersInRoomLabel.TabIndex = 42;
            // 
            // roundTimeSecondsLabel
            // 
            this.roundTimeSecondsLabel.AutoSize = true;
            this.roundTimeSecondsLabel.BackColor = System.Drawing.Color.Transparent;
            this.roundTimeSecondsLabel.Font = new System.Drawing.Font("MV Boli", 15F, System.Drawing.FontStyle.Bold);
            this.roundTimeSecondsLabel.ForeColor = System.Drawing.Color.Transparent;
            this.roundTimeSecondsLabel.Location = new System.Drawing.Point(597, 189);
            this.roundTimeSecondsLabel.Name = "roundTimeSecondsLabel";
            this.roundTimeSecondsLabel.Size = new System.Drawing.Size(0, 26);
            this.roundTimeSecondsLabel.TabIndex = 42;
            // 
            // updateRoomsTimer
            // 
            this.updateRoomsTimer.Interval = 1500;
            this.updateRoomsTimer.Tick += new System.EventHandler(this.updateRoomsTimer_Tick);
            // 
            // goBackButton
            // 
            this.goBackButton.BackColor = System.Drawing.Color.Transparent;
            this.goBackButton.BackgroundImage = global::DavidClient.Properties.Resources.go_back;
            this.goBackButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.goBackButton.Location = new System.Drawing.Point(0, -2);
            this.goBackButton.Name = "goBackButton";
            this.goBackButton.Size = new System.Drawing.Size(69, 63);
            this.goBackButton.TabIndex = 60;
            this.goBackButton.TabStop = false;
            this.goBackButton.Click += new System.EventHandler(this.goBackButton_Click);
            // 
            // roomList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.goBackButton);
            this.Controls.Add(this.roundTimeSecondsLabel);
            this.Controls.Add(this.playersInRoomLabel);
            this.Controls.Add(this.maxPlayersLabel);
            this.Controls.Add(this.playersInRoomHeadlineLabel);
            this.Controls.Add(this.joinButton);
            this.Controls.Add(this.roomsPanel);
            this.Controls.Add(this.refreshButton);
            this.Controls.Add(this.label1);
            this.Name = "roomList";
            this.Text = "roomList";
            this.Load += new System.EventHandler(this.roomList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.goBackButton)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel roomsPanel;
        private System.Windows.Forms.Button refreshButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button joinButton;
        private System.Windows.Forms.Label playersInRoomHeadlineLabel;
        private System.Windows.Forms.Label maxPlayersLabel;
        private System.Windows.Forms.Label playersInRoomLabel;
        private System.Windows.Forms.Label roundTimeSecondsLabel;
        private System.Windows.Forms.Timer updateRoomsTimer;
        private System.Windows.Forms.PictureBox goBackButton;
    }
}