﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DavidClient
{
    public partial class Game : Form
    {
        // General vars
        string _username;
        string _gameRoomName;

        // Game vars
        int _playerPanelsCounter = 0;
        Board _mainBoard;
        Dictionary<string, Board> _otherPlayers;
        Panel[] _otherPlayerPanels = new Panel[3];
        Label[] _otherPlayerLabels = new Label[3];
        string[] _playersNames;
        Socket _clientSocket;
        private Thread _waiter;
        private bool _keepRunning = true;

        // Timer
        private int _secondsCount = 0;
        private int _minutesCount = 0;

        public Game(string username, string gameRoomName, string puzzle, string[] playersNames, Socket clientSocket)
        {
            _username = username;
            _gameRoomName = gameRoomName;
            _playersNames = playersNames;
            _clientSocket = clientSocket;
            _otherPlayers = new Dictionary<string, Board>();

            InitializeComponent();
            initOtherPlayers();

            timer1.Enabled = true;
            timer1.Start();
            this._mainBoard = new Board(_gameRoomName, _username, this.mainGamePanel, true, puzzle, _clientSocket);

            for(int i = 0; i < _playersNames.Length; i++)
            {
                if(_playersNames[i] != _username)
                {
                    _otherPlayers[_playersNames[i]] = new Board(_gameRoomName, _username, _otherPlayerPanels[_playerPanelsCounter], false, puzzle, _clientSocket);
                    _otherPlayerLabels[_playerPanelsCounter].Text = _playersNames[i];
                    _playerPanelsCounter++;
                }
            }

            nameLabel.Text = username;

            _waiter = new Thread(new ThreadStart(listenToBoardChanges));
            _waiter.Start();
        }
        private void initOtherPlayers()
        {
            _otherPlayerPanels[0] = this.playerPanel1;
            _otherPlayerPanels[1] = this.playerPanel2;
            _otherPlayerPanels[2] = this.playerPanel3;

            _otherPlayerLabels[0] = this.otherPlayerLabel1;
            _otherPlayerLabels[1] = this.otherPlayerLabel2;
            _otherPlayerLabels[2] = this.otherPlayerLabel3;
        }

        private void listenToBoardChanges()
        {
            while (_clientSocket.Connected && _keepRunning)
            {
                byte[] reciveBuffer = new byte[1024];
                int rec = _clientSocket.Receive(reciveBuffer);
                byte[] temp = new byte[rec];
                Array.Copy(reciveBuffer, temp, rec);
                string returnFormServer = Encoding.ASCII.GetString(temp);
                string[] msgParts = returnFormServer.Split(';');

                if(!_keepRunning)
                {
                    break;
                }
                else if(msgParts[0] == "CLOSE_GAME_SUCCESS")
                {
                    _keepRunning = false;
                }
                else if (msgParts[0] == "UPDATE_GAME_PUZZLES_NOTIFY")
                {
                    insertBoardChanges(msgParts[1]);
                }
            }
        }

        private void insertBoardChanges(string newPlayerChanges)
        {
            if (InvokeRequired) // If there's a call from an external function (such as a thread)
            {
                Invoke(new MethodInvoker(delegate { insertBoardChanges(newPlayerChanges); })); // Does an additional call, now not from the outside
                return;
            }

            string[] playerStructs = newPlayerChanges.Split(':');

            for(int i =0; i < playerStructs.Length; i++) 
            {
                string name = playerStructs[i].Split('|')[0];
                string newPuzzle = playerStructs[i].Split('|')[1];

                if(name != _username)
                {
                    _otherPlayers[name].updatePuzzle(newPuzzle);
                }
            }
        }

        private bool check_sudoku_completed()
        {
            string occuringChars = "";

            // Check Rows
            for (int i = 0; i < 9; i++)
            {
                occuringChars = "";
                for (int j = 0; j < 9; j++)
                {
                    char currentChr = Char.Parse(this._mainBoard.getCells()[i, j].Text);
                    if (occuringChars.Contains(currentChr))
                    {
                        return false;
                    }
                    else
                    {
                        occuringChars += currentChr;
                    }
                }
            }

            // Check Columns
            for (int i = 0; i < 9; i++)
            {
                occuringChars = "";
                for (int j = 0; j < 9; j++)
                {
                    char currentChr = Char.Parse(this._mainBoard.getCells()[j, i].Text);
                    if (occuringChars.Contains(currentChr))
                    {
                        return false;
                    }
                    else
                    {
                        occuringChars += currentChr;
                    }
                }
            }

            // Check 3x3 squares
            for (int i = 0; i < 9; i += 3)
            {
                for (int j = 0; j < 9; j += 3)
                {
                    occuringChars = "";
                    for (int k = i; k < i + 3; k++)
                    {
                        for (int l = j; l < j + 3; l++)
                        {
                            char currentChr = Char.Parse(this._mainBoard.getCells()[k, l].Text);

                            if (occuringChars.Contains(currentChr))
                            {
                                return false;
                            }
                            else
                            {
                                occuringChars += currentChr;
                            }
                        }
                    }
                }
            }

            return true;
        }

        private void SubmitBoardButton_Click(object sender, EventArgs e)
        {
            //if (_mainBoard.getPuzzle().Contains('.'))
            //    return;

            //if (!check_sudoku_completed())
            //    return;

            timer1.Stop();

            sendCloseGameRequest();

            // If sudoku board is completed correctly
            _keepRunning = false;
            this.Hide();
            ResultsScreen resultsScreenForm = new ResultsScreen(_gameRoomName, _username, _clientSocket);
            resultsScreenForm.Closed += (s, args) => this.Close();
            resultsScreenForm.Show();

            sendPlayerTimerState();
        }
        private void sendCloseGameRequest()
        {
            string request = createCloseGameRequest();
            byte[] info = Encoding.ASCII.GetBytes(request);
            _clientSocket.Send(info);
        }
        private string createCloseGameRequest()
        {
            return "CLOSE_GAME_REQUEST";
        }
        private void sendPlayerTimerState()
        {
            string pressedKeyMessage = createUpdatePlayerTimerMessage();
            byte[] userInfo = Encoding.ASCII.GetBytes(pressedKeyMessage);
            _clientSocket.Send(userInfo);
        }

        private string createUpdatePlayerTimerMessage()
        {
            return "PLAYER_UPDATE_TIMER_REQUEST;" +  _gameRoomName + ";" + _username + ";" + timeLabel.Text;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            _secondsCount++;
            
            if(_secondsCount == 60)
            {
                _secondsCount = 0;
                _minutesCount++;
            }

            string secondsStr = _secondsCount < 10 ? "0" + _secondsCount.ToString() : _secondsCount.ToString();
            string minutesCount = _minutesCount < 10 ? "0" + _minutesCount.ToString() : _minutesCount.ToString();
            string timerText = minutesCount + ":" + secondsStr;

            timeLabel.Text = timerText;
        }

        private void Game_Load(object sender, EventArgs e)
        {

        }
    }
}
