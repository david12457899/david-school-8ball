﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DavidClient
{
    public partial class RoomListForm : Form
    {
        private string _currentRoomName;
        private string _username;
        private string[] _rooms;
        private Socket _clientSocket;

        // threading
        private Thread _waiter;
        private bool _keepRunning = true;
        
        public RoomListForm(string username, Socket clientSocket)
        {
            InitializeComponent();
            
            _clientSocket = clientSocket;
            _username = username;
            _currentRoomName = "";
            _rooms = new string[0];

            _waiter = new Thread(new ThreadStart(listenRoomsChanges));
            _waiter.Start();

            sendAddRoomsViewingUserRequest();
            updateRooms();
        }

        private void listenRoomsChanges()
        {
            while(_clientSocket.Connected && _keepRunning)
            {
                byte[] reciveBuffer = new byte[1024];
                int rec = _clientSocket.Receive(reciveBuffer);
                byte[] temp = new byte[rec];
                Array.Copy(reciveBuffer, temp, rec);
                string returnFormServer = Encoding.ASCII.GetString(temp);
                string[] msgParts = returnFormServer.Split('#');

                if(!_keepRunning)
                {
                    break;
                }
                else if(msgParts[0] == "REMOVE_ROOMS_VIEWING_USER_SUCCESS")
                {
                    _keepRunning = false;
                }
                else if(msgParts[0] == "ROOM_JOIN_OK")
                {
                    handleJoinRoom();
                }
                else if(msgParts[0] == "VIEW_ROOMS_NOTIFY")
                {
                    handleRoomsChange(msgParts[1]);
                }
            }
        }
        private void handleJoinRoom()
        {
            if (InvokeRequired) // If there's a call from an external function (such as a thread)
            {
                Invoke(new MethodInvoker(delegate { handleJoinRoom(); })); // Does an additional call, now not from the outside
                return;
            }

            int maxPlayers = -1;

            for (int i = 0; i < this._rooms.Length; i++)
            {
                if (this._rooms[i].Split(';')[0] == this._currentRoomName)
                {
                    maxPlayers = int.Parse(this._rooms[i].Split(';')[1]);
                }
            }

            _keepRunning = false;
            _waiter.Abort();
            updateRoomsTimer.Stop();
            this.Hide();
            waitingRoomMember create = new waitingRoomMember(this._username, this._currentRoomName, maxPlayers, _clientSocket);
            create.Closed += (s, args) => this.Close();
            create.Show();
        }
        private void handleRoomsChange(string updatedRooms)
        {
            if (InvokeRequired) // If there's a call from an external function (such as a thread)
            {
                Invoke(new MethodInvoker(delegate { handleRoomsChange(updatedRooms); })); // Does an additional call, now not from the outside
                return;
            }
            bool chosenRoomFound = false;

            roomsPanel.Controls.Clear();
            this._rooms = updatedRooms.Split(':');

            foreach (string current in this._rooms)
            {
                string[] room = current.Split(';');
                Label currentLabel = new Label();

                currentLabel.Name = room[0];
                currentLabel.Text = room[0];
                currentLabel.Click += new EventHandler(chooseRoom);
                currentLabel.Size = new Size(373, 30);
                currentLabel.Font = new Font("Britanic", 20, FontStyle.Bold);
                currentLabel.Location = new Point(0, roomsPanel.Controls.Count * 30);

                if (currentLabel.Name == this._currentRoomName)
                {
                    currentLabel.BackColor = Color.Blue;
                    chosenRoomFound = true;
                }

                roomsPanel.Controls.Add(currentLabel);
            }

            if (!chosenRoomFound)
            {
                this._currentRoomName = "";
            }
        }
        private void chooseRoom(object sender, EventArgs e) // Selects the chosen room
        {
            Label room = sender as Label;
            if (room != null)
            {
                foreach (Label current in roomsPanel.Controls)
                {
                    current.BackColor = Color.Transparent;
                    current.ForeColor = Color.Black;
                }
                room.BackColor = Color.BlueViolet;
                room.ForeColor = Color.White;
                this._currentRoomName = room.Text;
                revealRoomDetails(this._currentRoomName);
            }

        }

        private bool foundInPanel(Label room)
        {
            foreach (Label current in roomsPanel.Controls)
            {
                if(current.Name == room.Name)
                {
                    return true;
                }
            }
            return false;
        }

        private bool oldRoomsInNew(string[] oldRooms, string[] newRooms)
        {
            for(int i = 0; i < oldRooms.Length; i++)
            {
                if(!(Array.Exists(newRooms, element => element == oldRooms[i])))
                {
                    return false;
                }
            }
            return true;
        }

        private void revealRoomDetails(string name)
        {
            if(this._currentRoomName != "")
            {
                foreach (string room in _rooms)
                {
                    string[] current = room.Split(';');
                    if (current[0] == name)
                    {
                        maxPlayersLabel.Text = "Max Players: " + current[1]; 
                        string[] players = current[2].Split('|');

                        string buildString = "";
                        foreach (string player in players)
                        {
                            buildString += (player + "\n");
                        }
                        playersInRoomLabel.Text = buildString;
                    }
                }
            }
        }
        private string createViewRoomsRequest()
        {
            return "VIEW_ROOMS_REQUEST";
        }
        private void updateRooms()
        {
            // Send View Rooms Request to The Server
            string viewRoomsRequest = createViewRoomsRequest();
            byte[] requestResult = Encoding.ASCII.GetBytes(viewRoomsRequest);
            _clientSocket.Send(requestResult);
        }
        private string createJoinRoomRequest(string roomName, string user)
        {
            return "JOIN_ROOM_REQUEST" + ";" + this._currentRoomName + ";" + this._username;
        }
        private void joinButton_Click(object sender, EventArgs e)
        {
            if (this._currentRoomName != "")
            {
                // Send to the server join room request
                string joinRoomMsg = createJoinRoomRequest(this._currentRoomName, this._username);
                byte[] joinRoomResponse = Encoding.ASCII.GetBytes(joinRoomMsg);
                _clientSocket.Send(joinRoomResponse);

            }
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            //updateRooms();
        }

        private void updateRoomsTimer_Tick(object sender, EventArgs e)
        {
            //updateRooms();
        }

        private void roomList_Load(object sender, EventArgs e)
        {

        }

        private void goBackButton_Click(object sender, EventArgs e)
        {
            sendRemoveRoomsViewingUserReqest();

            _keepRunning = false;
            this.Hide();
            MainMenu mainMenuForm = new MainMenu(_username, _clientSocket);
            mainMenuForm.Closed += (s, args) => this.Close();
            mainMenuForm.Show();
        }

        private void sendRemoveRoomsViewingUserReqest()
        {
            string request = createRemoveRoomsViewingUserReqest();
            byte[] info = Encoding.ASCII.GetBytes(request);
            _clientSocket.Send(info);
        }

        private string createRemoveRoomsViewingUserReqest()
        {
            return "REMOVE_ROOMS_VIEWING_USER_REQUEST;" + _username;
        }

        private void sendAddRoomsViewingUserRequest()
        {
            string request = createAddRoomsViewingUserRequest();
            byte[] info = Encoding.ASCII.GetBytes(request);
            _clientSocket.Send(info);
        }

        private string createAddRoomsViewingUserRequest()
        {
            return "ADD_ROOMS_VIEWING_USER_REQUEST;" + _username;
        }
    }
}
