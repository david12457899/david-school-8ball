﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace DavidClient
{
    public partial class waitingRoomAdmin : Form
    {
        private Thread _updateRoomThread;
        private string _playerName;
        private string _roomName;
        private int _maxPlayers;
        private string[] _playersInRoom;
        private Socket _clientSocket;
        private Thread _waiter;
        private bool _running = true;
        private void initLabels()
        {
            this.roomNameLabel.Text = "Room: " + this._roomName;
            this.maxPlayersLabel.Text = "Max Players: " + this._maxPlayers.ToString();
            this.nameLabel.Text = "Name: " + this._playerName;
        }

        private void showPeopleInRoom(string[] names)
        {
            if (InvokeRequired) // If there's a call from an external function (such as a thread)
            {
                Invoke(new MethodInvoker(delegate { showPeopleInRoom(names); })); // Does an additional call, now not from the outside
                return;
            }
            participantsPanel.Controls.Clear();
            foreach (string name in names)
            {
                Label currentLabel = new Label();

                currentLabel.Name = name;
                currentLabel.Text = name;
                currentLabel.Size = new Size(375, 30);
                currentLabel.Font = new Font("Britanic", 20, FontStyle.Bold);
                currentLabel.Location = new Point(0, participantsPanel.Controls.Count * 30);

                participantsPanel.Controls.Add(currentLabel);

            }
        }
        public waitingRoomAdmin(string playerName, string roomName, int maxPlayers, Socket clientSocket)
        {
            _updateRoomThread = new Thread(updatePeopleInRoom);

            InitializeComponent();

            _clientSocket = clientSocket;
            this._playerName = playerName;
            this._roomName = roomName;
            this._maxPlayers = maxPlayers;

            initLabels();
            updatePeopleInRoom();
            _waiter = new Thread(new ThreadStart(listenToRoomChanges));
            _waiter.Start();
        }

        private string createPeopleInRoomMessage(string roomName)
        {
            return "GET_PEOPLE_IN_ROOM" + ";" + roomName;
        }

        private void startUpdatingPeopleInRoom()
        {
            // Send a message to the server to create a thread that updates the players

        }

        private void updatePeopleInRoom()
        {
            // Send update room requet to server
            string updateRoomRequest = createPeopleInRoomMessage(this._roomName);
            byte[] updateRoom = Encoding.ASCII.GetBytes(updateRoomRequest);
            _clientSocket.Send(updateRoom);

            // Receive message back from server
            byte[] buffer = new byte[1024];
            int rec = _clientSocket.Receive(buffer);
            byte[] temp = new byte[rec];
            Array.Copy(buffer, temp, rec);
            string serverResult = Encoding.ASCII.GetString(temp);

            if (serverResult != "GET_PEOPLE_IN_ROOM_FAILURE")
            {
                _playersInRoom = serverResult.Split(';');
                showPeopleInRoom(_playersInRoom);
            }
        }

        private string createCloseRoomMessage()
        {
            return "CLOSE_ROOM" + ";" + _roomName;
        }
        private void sendAdminCloseRoomMessage()
        {
            // Send admin close room request to server
            string adminCloseRoomRequest = createCloseRoomMessage();
            byte[] closeRoom = Encoding.ASCII.GetBytes(adminCloseRoomRequest);
            _clientSocket.Send(closeRoom);

            // Receive update room response from server
            byte[] buffer = new byte[1024];
            int rec = _clientSocket.Receive(buffer);
            byte[] temp = new byte[rec];
            Array.Copy(buffer, temp, rec);
            string serverResult = Encoding.ASCII.GetString(temp);
        }

        private string createStartGameMessage()
        {
            string msg = "START_GAME_REQUEST" + ";" + _roomName + ";" + _playerName;

            return msg;
        }

        private void sendStartGameMessage()
        {
            string startGameRequest = createStartGameMessage();
            byte[] startGame = Encoding.ASCII.GetBytes(startGameRequest);
            _clientSocket.Send(startGame);

            // Receive update room response from server
            /*
            byte[] buffer = new byte[1024];
            int rec = _clientSocket.Receive(buffer);
            byte[] temp = new byte[rec];
            Array.Copy(buffer, temp, rec);
            string serverResult = Encoding.ASCII.GetString(temp);

            
            else
            {
                // TODO: handle start game failure
            }*/


        }

        private void startGameButton_Click(object sender, EventArgs e)
        {
            sendStartGameMessage();
        }

        private void closeRoomButton_Click(object sender, EventArgs e)
        {

        }

        private void startGameForm(string serverResponse)
        {
            if (InvokeRequired) // If there's a call from an external function (such as a thread)
            {
                Invoke(new MethodInvoker(delegate { startGameForm(serverResponse); })); // Does an additional call, now not from the outside
                return;
            }

            string[] msgParts = serverResponse.Split(';');

            string puzzle = msgParts[1];
            string[] playerNames = msgParts[2].Split(':');

            this.Hide();
            Game gameForm = new Game(this._playerName, this._roomName, puzzle, playerNames, _clientSocket);
            gameForm.Closed += (s, args) => this.Close();
            gameForm.Show();
        }

        private void listenToRoomChanges()
        {
            byte[] reciveBuffer = new byte[1024];
            while (_clientSocket.Connected && _running)
            {
                int rec = _clientSocket.Receive(reciveBuffer);
                byte[] temp = new byte[rec];
                Array.Copy(reciveBuffer, temp, rec);
                string returnFormServer = Encoding.ASCII.GetString(temp);
                string[] msgParts = returnFormServer.Split(';');

                if(msgParts[0] == "ADMIN_LEAVE_SUCCESS")
                {
                    _running = false;
                }
                if (msgParts[0] == "UPDATE_PLAYERS_NOTIFY")
                {
                    showPeopleInRoom(msgParts[1].Split(':'));
                }
                else if (msgParts[0] == "START_GAME_SUCCESS")
                {
                    _running = false;
                    startGameForm(returnFormServer);
                }
            }
        }

        private void goBackButton_Click(object sender, EventArgs e)
        {
            sendAdminLeaveRequest();

            this.Hide();
            MainMenu mainMenuForm = new MainMenu(_playerName, _clientSocket);
            mainMenuForm.Closed += (s, args) => this.Close();
            mainMenuForm.Show();
        }

        private void sendAdminLeaveRequest()
        {
            string request = createAdminLeaveRequest();
            byte[] info = Encoding.ASCII.GetBytes(request);
            _clientSocket.Send(info);
        }

        private string createAdminLeaveRequest()
        {
            return "ADMIN_LEAVE_REQUEST;" + _roomName + ";" + _playerName;
        }
    }
}
