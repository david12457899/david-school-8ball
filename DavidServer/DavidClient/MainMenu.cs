﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace DavidClient
{
    public partial class MainMenu : Form
    {
        private Socket _clientSocket;
        private string _name;
        public MainMenu(string name, Socket clientSocket)
        {  
            InitializeComponent();
            _clientSocket = clientSocket;
            _name = name;
            nameLabel.Text = name;
        }
        private void createRoomButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            CreateRoomMenu createRoomMenuForm = new CreateRoomMenu(this._name, _clientSocket);
            createRoomMenuForm.Closed += (s, args) => this.Close();
            createRoomMenuForm.Show();
        }

        private void joinRoomButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            RoomListForm roomListForm = new RoomListForm(this._name, _clientSocket);
            roomListForm.Closed += (s, args) => this.Close();
            roomListForm.Show();
        }

        private void goBackButton_Click(object sender, EventArgs e)
        {
            sendLogoutRequest();

            this.Hide();
            Login loginForm = new Login(_clientSocket);
            loginForm.Closed += (s, args) => this.Close();
            loginForm.Show();
        }

        private void sendLogoutRequest()
        {
            string request = createLogoutRequest();
            byte[] info = Encoding.ASCII.GetBytes(request);
            _clientSocket.Send(info);
        }

        private string createLogoutRequest()
        {
            return "LOGOUT_REQUEST;" + _name;
        }
    }
}
