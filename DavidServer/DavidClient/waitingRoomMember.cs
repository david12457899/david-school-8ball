﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DavidClient
{
    public partial class waitingRoomMember : Form
    {
        private string _playerName;
        private string _roomName;
        private int _maxPlayers;
        private bool _keepRunning;
        private string[] _playersInRoom;
        Thread _waiter;
        Panel _playerPanel;
        Socket _clientSocket;

        private void initLabels()
        {
            this.roomNameLabel.Text = "Room: " + this._roomName;
            this.maxPlayersLabel.Text = "Max Players: " + this._maxPlayers.ToString();
            this.nameLabel.Text = "Name: " + this._playerName;

            
        }

        private void showPeopleInRoom(string[] names)
        {
            _playersInRoom = names;
            _playerPanel.Controls.Clear();
            foreach (string name in names)
            {
                Label currentLabel = new Label();

                currentLabel.Name = name;
                currentLabel.Text = name;
                currentLabel.Size = new Size(375, 30);
                currentLabel.Font = new Font("Britanic", 20, FontStyle.Bold);
                currentLabel.Location = new Point(0, _playerPanel.Controls.Count * 30);

                if(InvokeRequired) // If there's a call from an external function (such as a thread)
                {
                    Invoke(new MethodInvoker(delegate { showPeopleInRoom(names); })); // Does an additional call, now not from the outside
                    return;
                }

                _playerPanel.Controls.Add(currentLabel);
            }
        }

        public waitingRoomMember(string playerName, string roomName, int maxPlayers, Socket clientSocket)
        {
            _keepRunning = true;
            InitializeComponent();
            _clientSocket = clientSocket;
            this._playerName = playerName;
            this._roomName = roomName;
            this._maxPlayers = maxPlayers;
            initLabels();
            initPlayersPanel();
            updatePeopleInRoom(); // only for the first time, then it updates automatically
            _waiter = new Thread(new ThreadStart(listenToRoomChanges));
            _waiter.Start();
        }
        private string createPeopleInRoomMessage(string roomName)
        {
            return "GET_PEOPLE_IN_ROOM" + ";" + roomName;
        }
        private void updatePeopleInRoom()
        {
            string updateRoomRequest = createPeopleInRoomMessage(this._roomName);
            byte[] updateRoom = Encoding.ASCII.GetBytes(updateRoomRequest);
            _clientSocket.Send(updateRoom);

            // Receive message back from server
            byte[] buffer = new byte[1024];
            int rec = _clientSocket.Receive(buffer);
            byte[] temp = new byte[rec];
            Array.Copy(buffer, temp, rec);
            string serverResult = Encoding.ASCII.GetString(temp);

            if (serverResult != "GET_PEOPLE_IN_ROOM_FAILURE")
            {
                showPeopleInRoom(serverResult.Split(';'));
            }
        }

        private void updatePlayersTimer_Tick(object sender, EventArgs e)
        {
            //updatePeopleInRoom();
        }

        private void startGameMember(string puzzle)
        {
            if (InvokeRequired) // If there's a call from an external function (such as a thread)
            {
                Invoke(new MethodInvoker(delegate { startGameMember(puzzle); })); // Does an additional call, now not from the outside
                return;
            }

            _keepRunning = false;
            this.Hide();
            Game gameForm = new Game(this._playerName, this._roomName, puzzle, _playersInRoom, _clientSocket);
            gameForm.Closed += (s, args) => this.Close();
            gameForm.Show();
        }

        private void listenToRoomChanges()
        {
            // TODO: Handle thread exit in the code
            byte[] reciveBuffer = new byte[1024];
            while (_clientSocket.Connected && _keepRunning)
            {
                int rec = _clientSocket.Receive(reciveBuffer);
                byte[] temp = new byte[rec];
                Array.Copy(reciveBuffer, temp, rec);
                string returnFormServer = Encoding.ASCII.GetString(temp);
                string[] msgParts = returnFormServer.Split(';');

                if(!_keepRunning)
                {
                    break;
                }
                else if(msgParts[0] == "MEMBER_LEAVE_SUCCESS")
                {
                    handleAdminLeave();
                }
                else if (msgParts[0] == "UPDATE_PLAYERS_NOTIFY")
                {
                    showPeopleInRoom(msgParts[1].Split(':'));
                }
                else if (msgParts[0] == "GAME_START_NOTIFY")
                {
                    startGameMember(msgParts[1]);
                }
                else if (msgParts[0] == "ROOM_CLOSED_NOTIFY")
                {
                    //TODO
                }
            }

            

        }

        private void handleAdminLeave()
        {
            if (InvokeRequired) // If there's a call from an external function (such as a thread)
            {
                Invoke(new MethodInvoker(delegate { handleAdminLeave(); })); // Does an additional call, now not from the outside
                return;
            }

            _keepRunning = false;

            this.Hide();
            MainMenu mainMenuForm = new MainMenu(_playerName, _clientSocket);
            mainMenuForm.Closed += (s, args) => this.Close();
            mainMenuForm.Show();
        }

        private void waitingRoomMember_Load(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
        }

        private void initPlayersPanel()
        {
            _playerPanel = new Panel();
            _playerPanel.Size = new Size(265, 253);
            _playerPanel.Location = new Point(37, 259);
            _playerPanel.AutoScroll = true;
            _playerPanel.BackColor = SystemColors.Control;
            this.Controls.Add(_playerPanel);
        }

        private void goBackButton_Click(object sender, EventArgs e)
        {
            sendExitRoomMemberRequest();

            _keepRunning = false;

            this.Hide();
            MainMenu mainMenuForm = new MainMenu(_playerName, _clientSocket);
            mainMenuForm.Closed += (s, args) => this.Close();
            mainMenuForm.Show();
        }

        private void sendExitRoomMemberRequest()
        {
            string exitRoomMemberMessage = createExitRoomMemberMessage();
            byte[] messageInfo = Encoding.ASCII.GetBytes(exitRoomMemberMessage);
            _clientSocket.Send(messageInfo);
        }

        private string createExitRoomMemberMessage()
        {
            return "EXIT_ROOM_MEMBER_REQUEST;" + _roomName + ";" + _playerName;
        }
    }
}
