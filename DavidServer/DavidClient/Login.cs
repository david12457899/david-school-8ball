﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DavidClient
{
    public partial class Login : Form  
    {
        private static Socket _clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        public Login(Socket clientSocket = null)
        {
            InitializeComponent();
            ConnectToServer(clientSocket);
        }

        private void ConnectToServer(Socket clientSocket)
        {
            if (clientSocket == null || !_clientSocket.Connected)
            {
                try
                {
                    _clientSocket.Connect(IPAddress.Loopback, 50000);
                }
                catch (SocketException)
                {
                    Console.WriteLine(":not connected.");
                }

            }
            else
            {
                _clientSocket = clientSocket;
            }
        }

        private string createLoginMessage(string user, string pass)
        {
            return "LOGIN_REQUEST" + ";" + user + ";" + pass;
        }

        private void sendLoginMessage(string user, string pass)
        {
            // Send login message to server
            string loginInfo = createLoginMessage(user, pass);
            byte[] userInfo = Encoding.ASCII.GetBytes(loginInfo);
            _clientSocket.Send(userInfo);

            byte[] buffer = new byte[1024];
            int rec = _clientSocket.Receive(buffer);
            byte[] temp = new byte[rec];
            Array.Copy(buffer, temp, rec);
            string serverResult = Encoding.ASCII.GetString(temp);

            if (serverResult == "LOGIN_SUCCESS")
            {
                this.Hide();
                MainMenu mainMenuForm = new MainMenu(userTextBox.Text, _clientSocket);
                mainMenuForm.Closed += (s, args) => this.Close();
                mainMenuForm.Show();
            }
            else if(serverResult != "USER_ALREADY_LOGGED")
            {
                errorLabel.Text = "Wrong username or password";
            }
            else
            {
                errorLabel.Text = "User already logged in";
            }

        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            if (userTextBox.Text != "" && passTextBox.Text != "")
            {
                sendLoginMessage(userTextBox.Text, passTextBox.Text);
            }
        }

        private void registerButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            register registerForm = new register(_clientSocket);
            registerForm.Closed += (s, args) => this.Close();
            registerForm.Show();
        }
    }
}
