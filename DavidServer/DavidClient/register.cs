﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DavidClient
{
    public partial class register : Form
    {
        private Socket _clientSocket;

        public register(Socket clientSocket)
        {
            InitializeComponent();
            _clientSocket = clientSocket;
        }

        private string createRegisterMessage(string user, string pass, string email, string phone)
        {
            return "REGISTER_REQUEST" + ";" + user + ";" + pass + ";" + email + ";" + phone;
        }

        private void sendRegisterMessage(string user, string pass, string email, string phone)
        {
            // Send register info to server
            string registerInfo = createRegisterMessage(user, pass, email, phone);
            byte[] userInfo = Encoding.ASCII.GetBytes(registerInfo);
            _clientSocket.Send(userInfo);

            // Receive Server Response
            byte[] buffer = new byte[1024];
            int rec = _clientSocket.Receive(buffer);
            byte[] temp = new byte[rec];
            Array.Copy(buffer, temp, rec);
            string serverResault = Encoding.ASCII.GetString(temp);

            if (serverResault == "REGISTER_SUCCESS")
            {
                this.Hide();
                Login gameForm = new Login(_clientSocket);
                gameForm.Closed += (s, args) => this.Close();
                gameForm.Show();
            }
            else
            {
                errorLabel.Text = "User already exists";
            }

        }

        public bool checkUser()
        {
            if(!userTextBox.Text.All(Char.IsLetterOrDigit))
            {
                errorLabel.Text = "Please make sure your username is alphanumeric";
                return false;
            }
            if (!passTextBox.Text.All(Char.IsLetterOrDigit))
            {
                errorLabel.Text = "Please make sure your password is alphanumeric";
                return false;
            }
            if (!(emailTextBox.Text.Contains('@') && emailTextBox.Text.Contains('.')))
            {
                errorLabel.Text = "Please make sure your email is in email format";
                return false;
            }
            if(!phoneTextBox.Text.All(Char.IsDigit))
            {
                errorLabel.Text = "Please make sure your phone is numeric";
                return false;
            }
            return true;
        }
        private void registerButton_Click(object sender, EventArgs e)
        {
            if (checkUser())
            {
                sendRegisterMessage(userTextBox.Text, passTextBox.Text, emailTextBox.Text, phoneTextBox.Text);
            }
        }

        private void goBackButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login gameForm = new Login(_clientSocket);
            gameForm.Closed += (s, args) => this.Close();
            gameForm.Show();
        }
    }
}
