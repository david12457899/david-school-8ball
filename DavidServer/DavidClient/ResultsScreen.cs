﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DavidClient
{
    public partial class ResultsScreen : Form
    {
        // Connection variables
        private Socket _clientSocket;
        private Thread _waiter;

        private string _username;
        private string _roomName;

        private bool _keepRunning = true;

        public ResultsScreen(string roomName, string username, Socket clientSocket)
        {
            InitializeComponent();

            _username = username;
            _roomName = roomName;
            _clientSocket = clientSocket;
            _waiter = new Thread(new ThreadStart(listenResultsChanges));
            _waiter.Start();

            // Todo: add request for player details
            sendViewResultsMessage();
        }
        private string createSendViewResultsMessage()
        {
            return "SEND_VIEW_RESULTS_MESSAGE;" + this._roomName + ";" + this._username;
        }
        private void sendViewResultsMessage()
        {
            string viewResultsMessage = createSendViewResultsMessage();
            byte[] userInfo = Encoding.ASCII.GetBytes(viewResultsMessage);
            _clientSocket.Send(userInfo);
        }

        private void listenResultsChanges()
        {
            while(_clientSocket.Connected && _keepRunning)
            {
                byte[] reciveBuffer = new byte[1024];
                int rec = _clientSocket.Receive(reciveBuffer);
                byte[] temp = new byte[rec];
                Array.Copy(reciveBuffer, temp, rec);
                string returnFormServer = Encoding.ASCII.GetString(temp);
                string[] msgParts = returnFormServer.Split(';');

                if(!_keepRunning)
                {
                    break;
                }
                else if(msgParts[0] == "RETURN_TO_MAIN_MENU_SUCCESS")
                {
                    _keepRunning = false;
                }
                else if (msgParts[0] == "UPDATE_GAME_RESULTS_NOTIFY")
                {
                    insertResultsChanges(msgParts[1]);
                }
            }
        }

        private void insertResultsChanges(string resultsChanges)
        {
            if (InvokeRequired) // If there's a call from an external function (such as a thread)
            {
                Invoke(new MethodInvoker(delegate { insertResultsChanges(resultsChanges); })); // Does an additional call, now not from the outside
                return;
            }

            string[] results = resultsChanges.Split('|');
            resultsPanel.Controls.Clear();
           
            foreach(string result in results)
            {
                string playerName = result.Split('#')[0];
                string playerTime = result.Split('#')[1];

                if(playerTime != "")
                {
                    Label currentLabel = new Label();

                    currentLabel.Name = playerName;
                    currentLabel.Text = playerName + " - " + playerTime;
                    currentLabel.Size = new Size(387, 82);
                    currentLabel.Font = new Font("Britanic", 20, FontStyle.Bold);
                    currentLabel.Location = new Point(0, resultsPanel.Controls.Count * 82);

                    resultsPanel.Controls.Add(currentLabel);
                }                
            }
        }

        private void roomsPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void goBackButton_Click(object sender, EventArgs e)
        {
            _keepRunning = false;

            sendReturnToMainMenuRequest();

            this.Hide();
            MainMenu resultsScreenForm = new MainMenu(_username, _clientSocket);
            resultsScreenForm.Closed += (s, args) => this.Close();
            resultsScreenForm.Show();
        }

        private void sendReturnToMainMenuRequest()
        {
            string request = createReturnToMainMenuRequest();
            byte[] info = Encoding.ASCII.GetBytes(request);
            _clientSocket.Send(info);
        }

        private string createReturnToMainMenuRequest()
        {
            return "RETURN_TO_MAIN_MENU_REQUEST";
        }
    }
}
