﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DavidServer
{
    class Room
    {
        private string _roomName;
        private int _maxPlayers;
        List<string> _members = new List<string>();

        public Room(string playerName, string name, int duration)
        {
            this._members.Add(playerName);
            this._roomName = name;
            this._maxPlayers = duration;
        }
        public string getRoomName()
        {
            return this._roomName;
        }
        public int getDuration()
        {
            return this._maxPlayers;
        }
        public List<string> getMembers()
        {
            return this._members;
        }

        public void addMember(string name)
        {
            this._members.Add(name);
        }
        private void removeMember(string name)
        {
            this._members.Remove(name);
        }
        

    }
}
