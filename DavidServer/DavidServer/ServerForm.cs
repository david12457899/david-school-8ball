﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace DavidServer
{
    public partial class ServerForm : Form
    {
        private static Socket _serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private static List<Socket> _ClientListSockets = new List<Socket>();
        private static byte[] _Buffer = new byte[1024];
        private Dictionary<string, Socket> _usersViewingRooms = new Dictionary<string, Socket>();

        // rooms
        private List<Game> _games = new List<Game>();

        // login vars
        List<User> _users = new List<User>();
        List<User> _loggedUsers = new List<User>();

        // sudoku vars
        List<Puzzle> _puzzles = new List<Puzzle>();

        public void generatePuzzlesFromTxtFile()
        {
            bool easy = true;

            string[] lines = System.IO.File.ReadAllLines(@"sudokus.txt");

            foreach (string line in lines)
            {
                if (line == "*HARD*")
                    easy = false;
                else if (line != "*EASY*")
                {
                    Puzzle newPuzzle = new Puzzle();
                    newPuzzle._puzzle = line;
                    newPuzzle._difficulty = easy ? "EASY" : "HARD";
                    this._puzzles.Add(newPuzzle);
                }
                    
            }
        }
        public void SaveInXml(string fileName="users.xml")
        {
            using (var stream = new FileStream(fileName, FileMode.Create))
            {
                var xml = new XmlSerializer(typeof(List<User>));
                xml.Serialize(stream, this._users);
            }
        }

        public string solveSudokuPuzzle(string puzzle)
        {
            return "";
        }

        public void saveSudokuInXml(string fileName="sudoku.xml")
        {
            using (var stream = new FileStream(fileName, FileMode.Create))
            {
                var xml = new XmlSerializer(typeof(List<Puzzle>));
                xml.Serialize(stream, this._puzzles);
            }
        }

        public List<User> LoadFromXml(string fileName = "users.xml")
        {
            using (var stream = new FileStream(fileName, FileMode.Open))
            {
                var xml = new XmlSerializer(typeof(List<User>));
                return (List<User>)xml.Deserialize(stream);
            }
        }
        public List<Puzzle> LoadSudokuFromXml(string fileName = "sudoku.xml")
        {
            using (var stream = new FileStream(fileName, FileMode.Open))
            {
                var xml = new XmlSerializer(typeof(List<Puzzle>));
                return (List<Puzzle>)xml.Deserialize(stream);
            }
        }
        public Puzzle generateBoard(string difficulty)
        {
            Puzzle newPuzzle = new Puzzle();

            Random rnd = new Random();
            int puzzleNumChosen = rnd.Next(150);

            newPuzzle = difficulty == "EASY" ? this._puzzles[puzzleNumChosen] : this._puzzles[puzzleNumChosen + 150];

            return newPuzzle;
        }
        private void makeGameUnplayable(Game game)
        {
            // So it won't show the game to other players
            game._started = true;
        }
        private void notifyAdminLeave(Game game, string adminName)
        {
            string msg = "ADMIN_LEAVE_NOTIFY";
            // TODO: remove it from games list and notify all players
            makeGameUnplayable(game);

            byte[] data = Encoding.ASCII.GetBytes(msg);

            for (int i = 0; i < game._playersInGame.Count; i++)
            {
                if(game._playersInGame[i]._playerName != adminName)
                {
                    game._playersInGame[i]._playerSocket.BeginSend(data, 0, data.Length, SocketFlags.None, new AsyncCallback(EndRecieveInformation), game._playersInGame[i]._playerSocket);
                }
            }
        }
        private void updateRoomForViewingPlayers(string playerName)
        {
            string request = "VIEW_ROOMS_NOTIFY#";

            request = createViewRoomsRequest();
            byte[] data = Encoding.ASCII.GetBytes(request);

            foreach(KeyValuePair<string, Socket> entry in _usersViewingRooms)
            {
                if(entry.Key != playerName)
                {
                    entry.Value.BeginSend(data, 0, data.Length, SocketFlags.None, new AsyncCallback(EndRecieveInformation), entry.Value);
                }
            }
        }
        private string createViewRoomsRequest()
        {
            string returnMsg = "";
            int roomsNum = this._games.Count;

            if (roomsNum > 0)
            {
                returnMsg = "VIEW_ROOMS_NOTIFY#";
                for (int i = 0; i < roomsNum; i++)
                {
                    if (!this._games[i]._started && this._games[i]._maxPlayers > this._games[i]._playersInGame.Count) // If game hasn't started and room is not full
                    {
                        returnMsg += this._games[i]._roomName;
                        returnMsg += ";";
                        returnMsg += this._games[i]._maxPlayers.ToString();
                        returnMsg += ";";

                        // Adding players to response
                        for (int j = 0; j < this._games[i]._playersInGame.Count; j++)
                        {
                            returnMsg += this._games[i]._playersInGame[j]._playerName;

                            if (j < this._games[i]._playersInGame.Count - 1)
                            {
                                returnMsg += "|";
                            }
                        }

                        if (i < roomsNum - 1)
                        {
                            returnMsg += ":";
                        }

                    }
                }
            }
            if (returnMsg == "")
                returnMsg = " ";

            return returnMsg;
        }
        private void updatePlayersNotify(Game game, string username)
        {
            string msg = "UPDATE_PLAYERS_NOTIFY;";

            for (int i = 0; i < game._playersInGame.Count; i++)
            {
                msg += game._playersInGame[i]._playerName;
                
                if(i != game._playersInGame.Count - 1)
                {
                    msg += ":";
                }
            }
            byte[] data = Encoding.ASCII.GetBytes(msg);
            for (int i = 0; i < game._playersInGame.Count; i++)
            {
                if(game._playersInGame[i]._playerName != username)
                {
                    game._playersInGame[i]._playerSocket.BeginSend(data, 0, data.Length, SocketFlags.None, new AsyncCallback(EndRecieveInformation), game._playersInGame[i]._playerSocket);
                }
            }  
        }
        
        private void gameStartNotify(Game game, string adminName)
        {
            string notifyMsg = "GAME_START_NOTIFY;" + game._puzzle._puzzle;
            byte[] data = Encoding.ASCII.GetBytes(notifyMsg);

            for (int i = 1; i < game._playersInGame.Count; i++) // Starts from 1 because the admin will always be the first one
            {
                if(game._playersInGame[i]._playerName != adminName)
                {
                    game._playersInGame[i]._playerSocket.BeginSend(data, 0, data.Length, SocketFlags.None, new AsyncCallback(EndRecieveInformation), game._playersInGame[i]._playerSocket);
                }
            }
        }
        private void removeViewingRoomsUser(Socket socket, string playerName)
        {
            _usersViewingRooms.Remove(playerName);
        }
        private void addViewingRoomsUser(Socket socket, string playerName)
        {
            _usersViewingRooms[playerName] = socket;
        }
        private void updatePlayerBoard(Game game, string playerName, string newPuzzle)
        {
            for (int i = 0; i < game._playersInGame.Count; i++)
            { 
                if(game._playersInGame[i]._playerName == playerName)
                {
                    game._playersInGame[i]._currentBoard._puzzle = newPuzzle;
                }
            }
        }
        private string sendPlayerScores(Game game, string playerName)
        {
            string builtMessage = "UPDATE_GAME_RESULTS_NOTIFY;";
            for (int i = 0; i < game._playersInGame.Count; i++)
            {
                builtMessage += game._playersInGame[i]._playerName;
                builtMessage += "#";
                builtMessage += game._playersInGame[i]._playerTime;

                if (i < game._playersInGame.Count - 1)
                {
                    builtMessage += "|";
                }
            }

            byte[] data = Encoding.ASCII.GetBytes(builtMessage);

            for (int i = 0; i < game._playersInGame.Count; i++)
            {
                if (game._playersInGame[i]._playerName != playerName)
                {
                    game._playersInGame[i]._playerSocket.BeginSend(data, 0, data.Length, SocketFlags.None, new AsyncCallback(EndRecieveInformation), game._playersInGame[i]._playerSocket);

                }
            }
            return builtMessage;
        }
        private string updatePlayerFinished(Game game, string playerName)
        {
            string builtMessage = "UPDATE_GAME_RESULTS_NOTIFY;";
            for (int i = 0; i < game._playersInGame.Count; i++)
            {
                builtMessage += game._playersInGame[i]._playerName;
                builtMessage += "#";
                builtMessage += game._playersInGame[i]._playerTime;

                if (i < game._playersInGame.Count - 1)
                {
                    builtMessage += "|";
                }
            }

            byte[] data = Encoding.ASCII.GetBytes(builtMessage);

            for (int i = 0; i < game._playersInGame.Count; i++)
            {
                if(game._playersInGame[i]._playerName != playerName)
                {
                    game._playersInGame[i]._playerSocket.BeginSend(data, 0, data.Length, SocketFlags.None, new AsyncCallback(EndRecieveInformation), game._playersInGame[i]._playerSocket);
                }
            }

            return builtMessage;
        }

        private string notifyBoardChange(Game game, string playerName)
        {
            string notifyMsg = "UPDATE_GAME_PUZZLES_NOTIFY;";

            for (int i = 0; i < game._playersInGame.Count; i++)
            {
                notifyMsg += game._playersInGame[i]._playerName;
                notifyMsg += "|";
                notifyMsg += game._playersInGame[i]._currentBoard._puzzle;

                if (i != game._playersInGame.Count - 1)
                {
                    notifyMsg += ":";
            
                }
            }

            byte[] data = Encoding.ASCII.GetBytes(notifyMsg);

            for (int i = 0; i < game._playersInGame.Count; i++)
            {
                game._playersInGame[i]._playerSocket.BeginSend(data, 0, data.Length, SocketFlags.None, new AsyncCallback(EndRecieveInformation), game._playersInGame[i]._playerSocket);
            }

            return notifyMsg;
        }

        private void updatePlayerTimer(Game game, string playerName, string playerTime)
        {
            for (int i = 0; i < game._playersInGame.Count; i++)
            {
                if(game._playersInGame[i]._playerName == playerName)
                {
                    game._playersInGame[i]._playerTime = playerTime;
                    break;
                }
            }
        }

        public User userExists(string userName, string password)
        {
            for(int i = 0; i < this._users.Count; i++)
            {
                if(this._users[i]._name == userName &&
                    this._users[i]._pass == password)
                {
                    return this._users[i];
                }
            }
            return null;
        }

        public User userExists(string userName)
        {
            for (int i = 0; i < this._users.Count; i++)
            {
                if (this._users[i]._name == userName)
                {
                    return this._users[i];
                }
            }
            return null;
        }

        private Game findGameByName(string gameName)
        {
            for(int i = 0; i < this._games.Count; i++)
            {
                if(gameName == this._games[i]._roomName)
                {
                    return this._games[i];
                }
            }
            return null;
        }

        public ServerForm()
        {
            InitializeComponent();
            if(File.Exists("users.xml"))
            {
                this._users = LoadFromXml();
            }
            if(File.Exists("sudoku.xml"))
            {
                this._puzzles = LoadSudokuFromXml();
            }
            else
            {
                // Load to XML from txt file
                generatePuzzlesFromTxtFile();
                saveSudokuInXml();
            }
            StartServer();
        }
        private void StartServer()
        {
            _serverSocket.Bind(new IPEndPoint(IPAddress.Any, 50000));
            _serverSocket.Listen(10);
            _serverSocket.BeginAccept(waitForNewConnection, null);
        }
        private void waitForNewConnection(IAsyncResult AR)
        {
            Socket socket = _serverSocket.EndAccept(AR);
            _ClientListSockets.Add(socket);
            socket.BeginReceive(_Buffer, 0, _Buffer.Length, SocketFlags.None, RecieveInformation, socket);
            _serverSocket.BeginAccept(waitForNewConnection, null);
        }
        private void RecieveInformation(IAsyncResult AR)
        {
            Socket socket = (Socket)AR.AsyncState;
            int recieve = socket.EndReceive(AR);
            byte[] dataBuff = new byte[recieve];
            Array.Copy(_Buffer, dataBuff, recieve);
            string text = Encoding.ASCII.GetString(dataBuff);

            returnInformationToClient(socket, text);
        }
        private void returnInformationToClient(Socket socket, string response)
        {
            string returnMsg = " ";
            string[] words = response.Split(';');
            if (words[0] == "LOGIN_REQUEST") // login
            {
                // login 
                string username = words[1];
                string pass = words[2];

                User user  = userExists(username, pass);

                if (user != null)
                {
                    if (_loggedUsers.Contains(user))
                    {
                        returnMsg = "USER_ALREADY_LOGGED";
                    }
                    else
                    {
                        _loggedUsers.Add(user);
                        returnMsg = "LOGIN_SUCCESS";
                    }
                }
                else
                {
                    returnMsg = "LOGIN_FAILURE";
                }
            }
            else if(words[0] == "LOGOUT_REQUEST")
            {
                string username = words[1];

                User user = userExists(username);

                _loggedUsers.Remove(user);

                returnMsg = "";
            }
            else if (words[0] == "REGISTER_REQUEST") // register
            {
                // register
                string username = words[1];
                string pass = words[2];
                string email = words[3];
                string phone = words[4];

                User user = userExists(username);

                if (user == null)
                {
                    User newUser = new User();
                    newUser._name = username;
                    newUser._pass = pass;
                    newUser._email = email;
                    newUser._phone = phone;

                    this._users.Add(newUser);
                    SaveInXml();

                    returnMsg = "REGISTER_SUCCESS";
                }
                else
                {
                    returnMsg = "REGISTER_FAILURE";
                }
            }
            else if (words[0] == "CREATE_ROOM_REQUEST") // create room
            {

                string roomName = words[1];
                string playerName = words[2];
                int maxPlayers = int.Parse(words[3]);
                string difficulty = words[4];

                Puzzle generatedPuzzle = generateBoard(difficulty);

                PlayerInGame gameCreator = new PlayerInGame(socket, generatedPuzzle, playerName);
                this._games.Add(new Game(roomName, gameCreator, generatedPuzzle, maxPlayers));

                updateRoomForViewingPlayers(playerName);
                returnMsg = "CREATE_ROOM_SUCCESS";
            }
            else if (words[0] == "VIEW_ROOMS_REQUEST") // view rooms
            {
                returnMsg = createViewRoomsRequest();
            }
            else if (words[0] == "JOIN_ROOM_REQUEST") // join room
            {
                string roomName = words[1];
                string userName = words[2];

                for (int i = 0; i < this._games.Count; i++)
                {
                    if (this._games[i]._roomName == roomName && !this._games[i]._started) // Find room and check if it hasn't started yet
                    {
                        if (this._games[i]._playersInGame.Count == this._games[i]._maxPlayers) // Check if room is full
                        {
                            returnMsg = "FULL";
                        }
                        else
                        {
                            this._games[i].addPlayer(new PlayerInGame(socket, this._games[i]._puzzle, userName));
                            updatePlayersNotify(this._games[i], userName);
                            returnMsg = "ROOM_JOIN_OK";
                        }
                    }
                }

                removeViewingRoomsUser(socket, userName);
            }
            else if (words[0] == "GET_PEOPLE_IN_ROOM") // Get people inside room
            {

                string roomName = words[1];
                string msg = "";

                for (int i = 0; i < this._games.Count; i++)
                {
                    if (this._games[i]._roomName == roomName && !this._games[i]._started)
                    {
                        for (int j = 0; j < this._games[i]._playersInGame.Count; j++)
                        {
                            msg += this._games[i]._playersInGame[j]._playerName;

                            if (j < this._games[i]._playersInGame.Count - 1)
                            {
                                msg += ";";
                            }
                        }
                    }
                }

                if (msg != "")
                    returnMsg = msg;
                else
                    returnMsg = "GET_PEOPLE_IN_ROOM_FAILURE";


            }

            else if (words[0] == "START_GAME_REQUEST") // Start game (request from admin)
            {
                string roomName = words[1];
                string adminName = words[2];

                Game game = findGameByName(roomName);

                if (game != null)
                {
                    game._started = true;

                    returnMsg = "START_GAME_SUCCESS";
                    returnMsg += ";";
                    returnMsg += game._puzzle._puzzle;
                    returnMsg += ";";

                    for (int i = 0; i < game._playersInGame.Count; i++)
                    {
                        returnMsg += game._playersInGame[i]._playerName;

                        if (i != game._playersInGame.Count - 1)
                        {
                            returnMsg += ":";
                        }
                    }

                    gameStartNotify(game, adminName);
                    updateRoomForViewingPlayers(adminName);
                }
                else
                {
                    // TODO: handle null
                }
            }

            else if (words[0] == "KEY_PRESSED_NOTIFY")
            {
                string roomName = words[1];
                string playerName = words[2];
                string newPuzzle = words[3];

                Game game = findGameByName(roomName);

                if (game != null)
                {
                    updatePlayerBoard(game, playerName, newPuzzle);
                    notifyBoardChange(game, playerName);
                    returnMsg = "";
                }
                else
                {
                    // TODO: HANDLE NULL
                }
            }

            else if (words[0] == "PLAYER_UPDATE_TIMER_REQUEST")
            {
                string gameRoomName = words[1];
                string playerName = words[2];
                string playerTime = words[3];

                Game game = findGameByName(gameRoomName);

                updatePlayerTimer(game, playerName, playerTime);
                returnMsg = updatePlayerFinished(game, playerName);
            }
            else if (words[0] == "SEND_VIEW_RESULTS_MESSAGE")
            {
                string roomName = words[1];
                string playerName = words[2];

                Game game = findGameByName(roomName);

                sendPlayerScores(game, playerName);
            }
            else if (words[0] == "EXIT_ROOM_MEMBER_REQUEST")
            {
                string gameRoomName = words[1];
                string playerName = words[2];

                Game game = findGameByName(gameRoomName);

                game.removePlayer(playerName);
                updatePlayersNotify(game, playerName);

                returnMsg = "MENBER_LEAVE_SUCCESS";
            }
            else if (words[0] == "REMOVE_ROOMS_VIEWING_USER_REQUEST")
            {
                string playerName = words[1];
                removeViewingRoomsUser(socket, playerName);
                returnMsg = "REMOVE_ROOMS_VIEWING_USER_SUCCESS";
            }
            else if (words[0] == "ADD_ROOMS_VIEWING_USER_REQUEST")
            {
                string playerName = words[1];
                addViewingRoomsUser(socket, playerName);
                returnMsg = "";
            }
            else if (words[0] == "ADMIN_LEAVE_REQUEST")
            {
                string gameRoomName = words[1];
                string adminName = words[2];

                Game game = findGameByName(gameRoomName);

                notifyAdminLeave(game, adminName);

                returnMsg = "ADMIN_LEAVE_SUCCESS";
            }
            else if (words[0] == "RETURN_TO_MAIN_MENU_REQUEST")
            {
                returnMsg = "RETURN_TO_MAIN_MENU_SUCCESS";
            }
            else if(words[0] == "CLOSE_GAME_REQUEST")
            {
                returnMsg = "CLOSE_GAME_SUCCESS";
            }
            Console.WriteLine("Sending back: " + returnMsg);

            byte[] data = Encoding.ASCII.GetBytes(returnMsg);

            if(returnMsg != "")
                socket.BeginSend(data, 0, data.Length, SocketFlags.None, new AsyncCallback(EndRecieveInformation), socket);
            socket.BeginReceive(_Buffer, 0, _Buffer.Length, SocketFlags.None, new AsyncCallback(RecieveInformation), socket);
        }
        private static void EndRecieveInformation(IAsyncResult AR)
        {
            Socket socket = (Socket)AR.AsyncState;
            socket.EndSend(AR);
        }
    }
}
