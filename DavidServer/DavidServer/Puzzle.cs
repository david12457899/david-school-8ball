﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DavidServer
{
    public class Puzzle
    { 
        public Puzzle()
        {

        }
        public Puzzle(Puzzle other)
        {
            _puzzle = other._puzzle;
            _difficulty = other._difficulty;
        }
        public string _puzzle { get; set; }
        public string _difficulty { get; set; }
    }
}
