﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DavidServer
{
    public class User
    {
        public string _name {get; set;}
        public string _pass { get; set; } 
        public string _email { get; set; }
        public string _phone { get; set; }
    }
}
