﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DavidServer
{
    class Game
    {
        public List<PlayerInGame> _playersInGame { get; set; }
        public Puzzle _puzzle { get; set; }
        public int _maxPlayers { get; set; }
        public bool _started { get; set; }
        public string _roomName { get; set; }

        public Game(string roomName, PlayerInGame gameCreator, Puzzle generatedPuzzle, int maxPlayers)
        {
            this._roomName = roomName;

            this._playersInGame = new List<PlayerInGame>();
            this._playersInGame.Add(gameCreator);

            this._puzzle = new Puzzle(generatedPuzzle);
            this._maxPlayers = maxPlayers;
            this._started = false;
        }
        public void addPlayer(PlayerInGame player)
        {
            this._playersInGame.Add(player);
        }
        public void removePlayer(string playerName)
        {
            for(int i = 0; i < _playersInGame.Count; i++)
            {
                if(_playersInGame[i]._playerName == playerName)
                { 
                    this._playersInGame.Remove(_playersInGame[i]);
                }
            }
            
        }
    }
}
