﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace DavidServer
{
    public class PlayerInGame
    {
        public PlayerInGame(Socket playerSocket, Puzzle currentBoard, string playerName, string playerTime = "")
        {
            this._playerSocket = playerSocket;
            this._currentBoard = currentBoard;
            this._playerName = playerName;
            this._playerTime = playerTime;
        }
        public Socket _playerSocket { get; set; }
        public Puzzle _currentBoard { get; set; }
        public string _playerName { get; set; }
        public string _playerTime { get; set; }

    }
}
